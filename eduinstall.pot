# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-02 19:15+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: usr/lib/academix/eduinstall/eduinstall.py:219
#, python-format
msgid "The package '%s' could not be installed"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:221
#, python-format
msgid "The package '%s' could not be removed"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:238
msgid "Another application is using APT:"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:322
#, python-format
msgid "%d ongoing actions"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:332
#, python-format
msgid "Cancel the task: %s"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:344
msgid "Waiting"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:344
msgid "Downloading"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:344
msgid "Running"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:344
msgid "Finished"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:356
#, python-format
msgid "Installing %s"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:358
#, python-format
msgid "Removing %s"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:360
msgid "Updating cache"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:362
#: usr/lib/academix/eduinstall/eduinstall.py:364
msgid "No role set"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:484
#: usr/lib/academix/eduinstall/eduinstall.py:820
msgid "Software Manager"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:505
msgid "_File"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:509
msgid "Close"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:513
msgid "_Edit"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:517
msgid "Preferences"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:521
msgid "Search in packages summary (slower search)"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:525
msgid "Search in packages description (even slower search)"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:529
msgid "Open links using the web browser"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:533
msgid "Search while typing"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:553
msgid "Software sources"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:557
msgid "_View"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:561
msgid "Available packages"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:565
msgid "Installed packages"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:572
msgid "_Help"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:576
#: usr/lib/academix/eduinstall/eduinstall.py:818
msgid "About"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:622
#: usr/lib/academix/eduinstall/eduinstall.py:632
#: usr/lib/academix/eduinstall/eduinstall.py:1155
#: usr/share/academix/eduinstall/eduinstall.glade.h:1
msgid "Categories"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:623
msgid "Please choose a category"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:624
#, python-format
msgid "%d packages are currently available"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:633
msgid "Please choose a sub-category"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:654
msgid "No ongoing actions"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:655
msgid "Active tasks:"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:658
#: usr/share/academix/eduinstall/eduinstall.glade.h:8
msgid "Show all results"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:784
msgid "Account information"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:786
msgid "Your community account"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:788
msgid "Fill in your account info to review applications"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:790
#: usr/share/academix/eduinstall/eduinstall.glade.h:16
msgid "Username:"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:791
#: usr/share/academix/eduinstall/eduinstall.glade.h:17
msgid "Password:"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:869
#: usr/lib/academix/eduinstall/eduinstall.py:1809
#: usr/lib/academix/eduinstall/eduinstall.py:1820
msgid "B"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:871
#: usr/lib/academix/eduinstall/eduinstall.py:1811
#: usr/lib/academix/eduinstall/eduinstall.py:1822
msgid "KB"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:873
#: usr/lib/academix/eduinstall/eduinstall.py:1813
#: usr/lib/academix/eduinstall/eduinstall.py:1824
msgid "MB"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:875
#: usr/lib/academix/eduinstall/eduinstall.py:1815
#: usr/lib/academix/eduinstall/eduinstall.py:1826
msgid "GB"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:897
msgid "Icon"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:901
msgid "Application"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:908
msgid "Score"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:930
msgid "Task"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:933
msgid "Status"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:936
msgid "Progress"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1032
msgid "See more reviews"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1074
#: usr/share/academix/eduinstall/eduinstall.glade.h:5
msgid "Screenshot"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1084
#: usr/share/academix/eduinstall/eduinstall.glade.h:6
msgid "Website"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1092
#: usr/lib/academix/eduinstall/eduinstall.py:1104
#: usr/lib/academix/eduinstall/eduinstall.py:1830
#: usr/share/academix/eduinstall/eduinstall.glade.h:12
msgid "Reviews"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1156
msgid "Featured"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1158
msgid "Age 3-6"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1161
msgid "Age 6-14"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1164
msgid "Age 14-18"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1167
msgid "Age 18+"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1170
msgid "Math"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1172
msgid "Physics"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1174
msgid "Biology"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1176
msgid "Chemistry"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1178
msgid "Teacher"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1180
msgid "Electronics"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1182
msgid "Geography"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1184
msgid "Genetics"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1186
msgid "Foreign languages"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1188
msgid "Programming"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1190
msgid "Architecture"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1192
msgid "Robotics"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1194
msgid "Statistics"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1196
msgid "Virtual laboratory"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1198
msgid "Medicine"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1200
msgid "Energy"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1202
msgid "Astronomy"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1204
msgid "Engineering"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1206
msgid "Online resources"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1208
msgid "Internet"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1210
msgid "Sound and video"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1212
msgid "Office"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1214
msgid "Graphics"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1216
msgid "3D"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1218
msgid "Drawing"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1220
msgid "Photography"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1222
msgid "Publishing"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1224
msgid "Scanning"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1226
msgid "Viewers"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1228
msgid "Games"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1230
msgid "Accessories"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1231
msgid "System tools"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1233
msgid "Fonts"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1234
msgid "All Packages"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1441
#: usr/lib/academix/eduinstall/eduinstall.py:1903
#, python-format
msgid "%d reviews"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1477
#, python-format
msgid "%d packages"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1711
#, python-format
msgid "Only results in category \"%s\" are shown."
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1722
msgid "Search results"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1761
msgid "Hate it"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1761
msgid "Not a fan"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1761
msgid "So so"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1761
msgid "Like it"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1761
msgid "Awesome!"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1779
msgid "Submit"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1780
msgid "Your review"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1828
msgid "Size:"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1829
msgid "Version:"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1831
msgid "Your review:"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1832
#: usr/share/academix/eduinstall/eduinstall.glade.h:4
msgid "Details"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1834
msgid "This will remove the following packages:"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1835
msgid "Cancel"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1836
msgid "Confirm"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1840
#, python-format
msgid "%(localSize)s of disk space freed"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1842
#, python-format
msgid "%(localSize)s of disk space required"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1845
#, python-format
msgid "%(downloadSize)s to download, %(localSize)s of disk space freed"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1847
#, python-format
msgid "%(downloadSize)s to download, %(localSize)s of disk space required"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1850
msgid "The following packages would be installed: "
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1852
msgid "The following packages would be removed: "
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1855
msgid "Impact on packages:"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1873
msgid "Remove"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1875
msgid "Installed"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1879
msgid "Not available"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1881
msgid "Please use apt-get to install this package."
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1884
msgid "Install"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1886
msgid "Not installed"
msgstr ""

#: usr/lib/academix/eduinstall/eduinstall.py:1907
msgid "Reviews:"
msgstr ""

#: usr/share/academix/eduinstall/eduinstall.glade.h:2
msgid "Mixed"
msgstr ""

#: usr/share/academix/eduinstall/eduinstall.glade.h:3
msgid "Packages"
msgstr ""

#: usr/share/academix/eduinstall/eduinstall.glade.h:7
msgid "Only results"
msgstr ""

#: usr/share/academix/eduinstall/eduinstall.glade.h:9
msgid "Search"
msgstr ""

#: usr/share/academix/eduinstall/eduinstall.glade.h:10
msgid "The following transactions are ongoing:"
msgstr ""

#: usr/share/academix/eduinstall/eduinstall.glade.h:11
msgid "Transactions"
msgstr ""

#: usr/share/academix/eduinstall/eduinstall.glade.h:13
msgid "<b>Your community account</b>"
msgstr ""

#: usr/share/academix/eduinstall/eduinstall.glade.h:14
msgid "http://community.linuxmint.com"
msgstr ""

#: usr/share/academix/eduinstall/eduinstall.glade.h:15
msgid "<i><small>Fill in your account info to review applications</small></i>"
msgstr ""
